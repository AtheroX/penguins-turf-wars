﻿namespace SharedLibrary.Requests; 

[Serializable]
public class CreatePlayerRequest {
    public string Name;
}