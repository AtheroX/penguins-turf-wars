﻿namespace SharedLibrary.Requests; 

[Serializable]
public class AuthRequests {
    public string Username;
    public string Password;
}