﻿using Microsoft.EntityFrameworkCore;

namespace SharedLibrary;

public class GameDBContext : DbContext {
    public DbSet<User> users { get; set; }
    public DbSet<Player> players { get; set; }

    public GameDBContext(DbContextOptions<GameDBContext> options) : base(options) {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<User>().HasOne(a => a.Player).WithOne(b => b.User).HasForeignKey<Player>(c => c.Id);
    }
}