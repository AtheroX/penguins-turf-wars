﻿using Microsoft.AspNetCore.Mvc;
using Server.Services;
using SharedLibrary;
using SharedLibrary.Requests;
using SharedLibrary.Responses;

namespace Server.Controllers; 

/// <summary>
/// Controller for authoritation (Login/Register)
/// </summary>
[ApiController]
[Route("[controller]")]
public class AuthController : ControllerBase{
    
    private readonly IAuthService _authService;

    public AuthController(IAuthService authService) {
        _authService = authService;
    }

    /// <summary>
    /// Register endpoint for creating a User
    /// </summary>
    /// <param name="req">Request for auth</param>
    /// <returns>Executes the login endpoint</returns>
    [HttpPost("register")]
    public IActionResult Register(AuthRequests req) {
        var (success, content) = _authService.Register(req.Username, req.Password);
        if (!success) return BadRequest(content);
        
        return Login(req);
    }
    
    /// <summary>
    /// Login endpoint for using a User
    /// </summary>
    /// <param name="req">Request for auth</param>
    /// <returns>Returns the state of petition, not success = BadRequest, if Ok returns the token of JWT</returns>
    [HttpPost("login")]
    public IActionResult Login(AuthRequests req) {
        var (success, content) = _authService.Login(req.Username, req.Password);
        if (!success) return BadRequest(content);

        return Ok(new AuthResponse(){token = content});
    }
    
}