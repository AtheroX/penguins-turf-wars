﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour{

    [HideInInspector] public Vector3[] points;    

    //Coger los waypoints y ponerlos en orden
    void Awake() {
        points = new Vector3[transform.childCount];
        
        /*Pone el ancho que se usará para el punto 1 del actual y después del primer punto será el 2o 
        * punto y el siguiente 1 */
        for (int i = 0; i < points.Length; i++) {
            points[i] = transform.GetChild(i).position;
        }
    }
    
}
