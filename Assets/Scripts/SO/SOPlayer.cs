using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Player", order = 0)]
public class SOPlayer : ScriptableObject {

	public int id;
	public string username;
	public int lvl;
	public int exp;

	public void CerrarSesion(bool Return = false) {
		id = 0;
		username = "";
		lvl = 1;
		exp = 0;
		if (Return)
			SceneUtils.GoToScene(0);
	}

}
