﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Mirror;

/// <summary>
/// Aunque se llama Boss debería llamarse JungleEnemy
/// </summary>
public class Boss : HittableMulti{

    [Header("Propiedades")]	
	public int damage = 2;
    public int moneyToDrop = 50;
	[Space]
    
	#region Hook
	[SyncVar(hook = nameof(HookSpeed))]
    public float speed = 1.5f;
    public void HookSpeed(float inutil, float actSpeed) {
        speed = actSpeed;
		if(agent!=null)
			agent.speed = speed;
    }
	#endregion

    private NavMeshAgent agent;
	[HideInInspector] public BossSpawn whoSpawnedMee;

	public Vector3 spawnPos;
	public bool haveIBeenHitted;
	public float distanceToAttack = 2f;
	public float timeToKalm = 3f;
	public float distanceToReturnToCampament = 15f;

	private PenguinResourcesMulti nearestObjective;
	private PenguinResourcesMulti lastHitMe;
    private LayerMask clickableMask;
	private Coroutine iHaveBeenHitted;

	/// <summary>
	/// Unity decide que no puedes desactivar componentes que no tienen Start
	/// </summary>
	void Start(){}

	/// <summary>
	/// Esta función hace de suplemento del Start
	/// </summary>
    public void SpawnEnemy() {
        TryGetComponent<NavMeshAgent>(out agent);
        agent.enabled = true;
        speed = agent.speed;
        ResetHP();
        clickableMask = LayerMask.GetMask("Clickable");

        CheckForNextWaypoint();
    }

	/// <summary>
	/// Todo el conjunto de waypoints en el boss no funcionan, pero está completamente basada de la clase de enemigo y no dió tiempo a limpiarla
	/// </summary>
    private void CheckForNextWaypoint() {
        StartCoroutine(CheckForNextWaypointC());
    }

	/// <summary>
	/// Si me han golpeado y está a rango voy a golpearlo
	/// </summary>
	/// <returns></returns>
    IEnumerator CheckForNextWaypointC() {
		while (true) {
			if (haveIBeenHitted) {
				if (CheckForAttack()) {
					AttackObjective();
				} else
					nearestObjective = null;
			}
			yield return new WaitForSeconds(0.1f);
		}
    }

	/// <summary>
	/// Comprueba si tiene algún pingüino jugador a rango, en caso de tener más de uno va a por el más cercano
	/// </summary>
	/// <returns>True = Puedo atacar</returns>
	public bool CheckForAttack() {
		Collider[] hits = Physics.OverlapSphere(transform.position, distanceToReturnToCampament, clickableMask);
		List<GameObject> hitteables = new List<GameObject>();
		foreach (Collider hit in hits) {
			if (hit.transform.TryGetComponent<PenguinResourcesMulti>(out PenguinResourcesMulti hitEn))
				hitteables.Add(hitEn.gameObject);
		}
		if(hitteables.Count > 0) {
			PenguinResourcesMulti miPadre = Utils.GetNearestTarget(hitteables, transform.position).GetComponent<PenguinResourcesMulti>();
			if (agent.isOnNavMesh) {
				nearestObjective = miPadre;
				agent.SetDestination(miPadre.transform.position);
				return true;
			}
			return false;
		}
		return false;
	}

	/// <summary>
	/// Si está a rango de ataque lo golpea
	/// </summary>
	private void AttackObjective() {
		print("ATACKgR");
		if (Vector3.Distance(transform.position, nearestObjective.transform.position) <= distanceToAttack) {
			nearestObjective.Hit(damage, this);
		} 
	}

	/// <summary>
	/// Al recibir un golpe parpadea
	/// </summary>
	/// <param name="dmg"></param>
	/// <param name="lastHitter"></param>
	/// <param name="skipInvulnerable"></param>
	/// <returns>True = Vivo</returns>
	public override bool Hit(float dmg, HittableMulti lastHitter, bool skipInvulnerable = false) {
		base.Hit((double)dmg, lastHitter, skipInvulnerable);
        if(lastHitter.GetType() == typeof(PenguinResourcesMulti))
            lastHitMe = (PenguinResourcesMulti)lastHitter;

        if (!haveIBeenHitted)
			iHaveBeenHitted = StartCoroutine(Kalm());

		Color hitColor = Color.red * Mathf.Pow(2, 1.2f);
		DamageEffectSequence(hitColor, .5f);
		StartCoroutine(DamageEffectSequenceOnServer(transform.GetChild(0).GetComponent<MeshRenderer>(), hitColor, .5f));

		return Alive();
	}

	/// <summary>
	/// Hace que tras estar a demasiada distancia de su spawn o cierto tiempo vuelva a su casita
	/// </summary>
	IEnumerator Kalm() {
		haveIBeenHitted = true;
		for(float t = 0; t < 1; t += Time.deltaTime / timeToKalm) {
            if(nearestObjective != null) 
                if(!nearestObjective.Alive())
                    break;
            
			if (Vector3.Distance(transform.position, spawnPos) >= distanceToReturnToCampament) 
				break;
			
			yield return null;
		}
		haveIBeenHitted = false;
        if(agent.isOnNavMesh)
		    agent.destination = spawnPos;
	}

    /// <summary>
    /// Boss muere y da dinero al jugador
    /// </summary>
    public override void Die(HittableMulti lastHitter) {
        if(lastHitter.GetType() == typeof(PenguinResourcesMulti)) {
            ((PenguinResourcesMulti)lastHitter).money += moneyToDrop;
            GameManager.instance.OnJungleMinionDie(((PenguinResourcesMulti)lastHitter).manager.imBlue);
        }
		whoSpawnedMee.Respawn();
        Destroy(this.gameObject); 
    }

	/// <summary>
	/// Pone los debuffs de la habilidad seleccionada
	/// Fuego quema y Tierra ralentiza
	/// </summary>
	/// <param name="skillElement">Elemento a añadir (Sólo pueden ser Tierra y Fuego)</param>
	/// <param name="byteOfSkill">Byte de la habilidad</param>
	public override void SetDebuff(ElementButton type, byte skillSelected){
        if (type == ElementButton.None) return;
        if (!actuallyBuffedOrDebuffed){
            actuallyBuffedOrDebuffed = true;
            SOHabilidad act = GameManager.instance.hud.GetSkillByByte(skillSelected);
            StartCoroutine(BuffsAndDebuffs(type, act));
        }
    }

    private Coroutine onFireCor;
	/// <summary>
	/// Pone todos los buffs y debuffs (en base a lo que recibe la función)
	/// Al cabo del tiempo que diga la habilidad resetea todo
	/// </summary>
	/// <param name="elementOfBuff">Elemento que va a aplicar</param>
	IEnumerator BuffsAndDebuffs(ElementButton type, SOHabilidad soBuffs) {
        float baseSpeed = speed;
        if(type == ElementButton.Earth) 
            speed *= soBuffs.slow;
        
        if(type == ElementButton.Fire)
            onFireCor = StartCoroutine(OnFire(soBuffs.quemadura));


        yield return new WaitForSeconds(soBuffs.buffent);

        if(type == ElementButton.Earth) 
            speed = baseSpeed;
        
        if(type == ElementButton.Fire)
            StopCoroutine(onFireCor);


        actuallyBuffedOrDebuffed = false;
    }

	/// <summary>
	/// Hace daño cada X tiempo ignorando la invlunerabilidad al nosotros mismos como si fuese quien nos quemó
	/// El while no es problema porque esto se guarda en una corrutina y al acabar su tuempo de actuación se para
	/// </summary>
	IEnumerator OnFire(float dmg) {
		while (true) {
			yield return new WaitForSeconds(.25f);
			Hit(dmg, lastHitMe, true);
		}
	}

	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position + new Vector3(0, .25f, 0), distanceToAttack);
		Gizmos.color = Color.black;
		Gizmos.DrawWireSphere(spawnPos, distanceToReturnToCampament);
	}
}
