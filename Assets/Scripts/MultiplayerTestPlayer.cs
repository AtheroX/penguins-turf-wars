///*
//* Copyright (c) AtheroX
//* https://twitter.com/athero_x
//*/

//using Mirror;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class MultiplayerTestPlayer : NetworkBehaviour{

//	public float speed = 10;
//	Rigidbody rb;
//	Transform cam;

//	public void Start() {
//		if (!isLocalPlayer) return;
//		rb = GetComponent<Rigidbody>();
//		cam = Camera.main.transform;

//		OnlyInMyClient();
//	}

//	[Client]
//	void OnlyInMyClient() {
//		GetComponent<Renderer>().material.color = Color.blue;
//		cam.parent = transform;
//		cam.position = transform.position+new Vector3(-11.5f,10,-7.5f);
//		cam.Rotate(new Vector3(35,57.5f,0));
//	}

//	void Update(){
//		if (!isLocalPlayer) return;
//		rb.velocity = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));

//	}

//	public override void OnStopClient() {
//		cam.parent = transform.parent;
//	}
//}
