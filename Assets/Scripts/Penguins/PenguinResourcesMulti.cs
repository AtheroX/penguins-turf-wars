using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class PenguinResourcesMulti : HittableMulti {

    [HideInInspector] public PenguinControllerMulti manager;
    public void SetManager(PenguinControllerMulti manager) => this.manager = manager;

	#region Hooks
	[SyncVar (hook = nameof(HookMaxMana))]
	public float maxMana;
	[SyncVar (hook = nameof(HookMana))]
    [ShowInInspector] private float mana;
    [SyncVar (hook = nameof(HookMoney))]
    public int money = 0;
	[SyncVar(hook = nameof(HookDead))]
	private bool dead;
	#endregion

	#region HP & Mana
	public float hpRegen;
    public float manaRegen;
    public float timeToRegen = 0.5f;
    [HideInInspector] public bool canRegenHP = false;
    private bool isRegeneratingHp = false;
    private Coroutine regenHpCoroutine;
    [HideInInspector] public bool canRegenMana = false;
    private bool isRegeneratingMana = false;
    private Coroutine regenManaCoroutine;

    public float extraRegenInsideNexusArea = 1.5f;
	#endregion

	private Vector3 spawnPosition;
    private PenguinResourcesMulti penguinWhoBurntMe;
	[HideInInspector] public GameObject selectableArea; // Circulo de selelccin de nuestro alrededor


	private void Start() {
		selectableArea = transform.GetChild(2).gameObject;
		ResetHP();
		UpdateHealth();
		spawnPosition = transform.position;
	}

	/// <summary>
	/// Manda a actualizar la vida en el HUD
	/// </summary>
	public void UpdateHealth() {
		if (!isLocalPlayer) return;
		GameManager.instance.hud.UpdateHP(hp, maxHp);
	}

	/// <summary>
	/// Manda a actualizar el Mana en el HUD
	/// </summary>
    public void UpdateMana() {
        if(!isLocalPlayer) return;
        GameManager.instance.hud.UpdateMana(mana, maxMana);
    }

	/// <summary>
	/// Manda a actualizar el dinero en el HUD
	/// </summary>
    public void UpdateMoney() {
        if(!isLocalPlayer) return;
        GameManager.instance.hud.UpdateMoney(money);
    }

	/// <summary>
	/// Calcula por distancia si deberas estar teniendo los buffos por estar dentro de tu barrera
	/// </summary>
	/// <returns>Devuelve el número de multiplicación de regeneración</returns>
    public float RegenInsideNexus() {
        return (Vector3.Distance(manager.myNexus.transform.position, transform.position) / GameManager.instance.GetNexus(manager.imBlue).transform.localScale.x <= manager.myNexus.barrierSize / 2 ? extraRegenInsideNexusArea : 1);
    }

	/// <summary>
	/// Comprueba si te falta vida o maná y en base a eso te hace regenerarlo
	/// Para regenerarlo tiene que pasar un tiempo desde que gastaste mana o perdiste vida, si te golpean o gastas maná
	/// ese tiempo se reinicia.
	/// </summary>
    [Command]
    public void Regenerate() {
		//print(transform.name + " regenerating at " + RegenInsideNexus());
        if(hp < maxHp) {
            if(canRegenHP) {
                hp += hpRegen * RegenInsideNexus() * Time.deltaTime;
            } else {
                if(!isRegeneratingHp) {
                    isRegeneratingHp = true;
                    regenHpCoroutine = StartCoroutine(RegenerateHp());
                }
            }
        }

        if(mana < maxMana) {
            if(canRegenMana) {
                mana += manaRegen * RegenInsideNexus() * Time.deltaTime;
            } else {
                if(!isRegeneratingMana) {
                    isRegeneratingMana = true;
                    regenManaCoroutine = StartCoroutine(RegenerateMana());
                }
            }
        }
    }

	/// <summary>
	/// Es el contador que sirve para saber si puedes regenerar vida
	/// </summary>
	private IEnumerator RegenerateHp() {
        yield return new WaitForSeconds(timeToRegen);
        canRegenHP = true;
        isRegeneratingHp = false;
    }

	/// <summary>
	/// Es el contador que sirve para saber si puedes regenerar mana
	/// </summary>
	private IEnumerator RegenerateMana() {
        yield return new WaitForSeconds(timeToRegen);
        canRegenMana = true;
        isRegeneratingMana = false;
    }

	/// <summary>
	/// Consume mana y reinicia el contador de tiempo
	/// </summary>
	/// <param name="cost">Coste de mana</param>
	[Command]
	public void ConsumeMana(float cost) {
        mana -= cost;
        canRegenMana = false;
        isRegeneratingMana = false;

        if(regenManaCoroutine != null)
            StopCoroutine(regenManaCoroutine);

    }

	/// <summary>
	/// Recibe daño, hace parpadear al pingüino y reinicia el contador de regeneración de vida
	/// </summary>
	/// <param name="dmg">daño a recibir</param>
	/// <param name="lastHitter">Quién golpea</param>
	/// <param name="skipInvulnerable">Si debe saltarse la invulnerabilidad del receptor</param>
	/// <returns>Si sigue vivo o no</returns>
    public override bool Hit(float dmg, HittableMulti lastHitter, bool skipInvulnerable = false) {
        base.Hit((double)dmg, lastHitter, skipInvulnerable);
        if(lastHitter.GetType() == typeof(PenguinResourcesMulti))
            penguinWhoBurntMe = (PenguinResourcesMulti)lastHitter;

        canRegenHP = false;
        isRegeneratingHp = false;
		Color hitColor = Color.red * Mathf.Pow(2, 2);

		DamageEffectSequence(hitColor, .5f);
		StartCoroutine(DamageEffectSequenceOnServer(transform.GetChild(0).GetComponent<MeshRenderer>(), hitColor, .5f));

		if (regenHpCoroutine != null)
            StopCoroutine(regenHpCoroutine);
        return Alive();
    }

	public void ResetMana() => mana = maxMana;

    public void FullReset() { ResetHP(); this.ResetMana(); }

    public bool HasMana(float manaCost) { return mana >= manaCost; }

	#region Funciones de Hook
	public override void HookHP(float inutil, float actHP) {
		hp = actHP;
		UpdateHealth();
	}

	public override void HookMaxHP(float inutil, float actHP) {
		maxHp = actHP;
		UpdateHealth();
	}

	public void HookMana(float inutil, float actMana) {
		mana = actMana;
		UpdateMana();
	}
	public void HookMaxMana(float inutil, float actMana) {
		maxMana = actMana;
		UpdateMana();
	}

	public void HookMoney(int inutil, int actMoney) {
        money = actMoney;
        UpdateMoney();
    }

	public void HookDead(bool inutil, bool actdead) {
		dead = actdead;
		if(actdead)
			StartCoroutine(DieAndRespawn(transform));
		else
			StartCoroutine(Respawn(transform));
	}
	#endregion

	/// <summary>
	/// Llamada al no tener vida.
	/// Genera un seguido de procesos gracias al Hook donde hace que muera en todos los clientes y que reaparezca al cabo de un rato
	/// </summary>
	/// <param name="lastHitter"></param>
	public override void Die(HittableMulti lastHitter = null) {
        print("He muerto " + transform.name);
		dead = true;
    }

	/// <summary>
	/// Corrutina llamada por el hook que hace que desaparezca visualmente y haga el tiempo de respawn
	/// </summary>
	/// <param name="transform">Objeto que desaparece</param>
	private IEnumerator DieAndRespawn(Transform transform) {
		transform.GetChild(0).gameObject.SetActive(false);
		print("Respawning " + transform.name);

		yield return new WaitForSeconds(5);
		dead = false;
	}

	/// <summary>
	/// Pone la vida al 100% y llama al respawn
	/// </summary>
	/// <param name="transform">Objeto a respawnear</param>
	private IEnumerator Respawn(Transform transform) {
        print("Respawneado ole ole " + transform.name);
		ResetHP();
		RespawnServer(transform);
		RespawnRPC(transform);
		yield return null;
	}

	/// <summary>
	/// Hace aparecer en todos los clientes y lo posiciona en su SPAWN
	/// </summary>
	/// <param name="transform">Objeto que reaparece</param>
	[ClientRpc]
	private void RespawnRPC(Transform transform) {
		transform.GetChild(0).gameObject.SetActive(true);
		manager.Move(spawnPosition, true);
	}

	[Server]
	private void RespawnServer(Transform transform) {
		transform.GetChild(0).gameObject.SetActive(true);
		manager.Move(spawnPosition, true);
	}

	/// <summary>
	/// Pone los debuffs de la habilidad seleccionada
	/// Fuego quema y Tierra ralentiza
	/// </summary>
	/// <param name="skillElement">Elemento a añadir (Sólo pueden ser Tierra y Fuego)</param>
	/// <param name="byteOfSkill">Byte de la habilidad</param>
	[Client]
	public override void SetDebuff(ElementButton skillElement, byte byteOfSkill) {
		if (skillElement != ElementButton.Earth && skillElement != ElementButton.Fire) return;
		if (!actuallyBuffedOrDebuffed) {
			actuallyBuffedOrDebuffed = true;
			SOHabilidad act = GameManager.instance.hud.GetSkillByByte(byteOfSkill);
			StartCoroutine(BuffsAndDebuffs(skillElement, act));
		}
	}

	/// <summary>
	/// Pone los buffs de la habilidad recibida
	/// Agua aumenta daño y Aire aumenta velocidad
	/// </summary>
	/// <param name="skillElement">Elemento a añadir (Sólo pueden ser Aire y Agua)</param>
	[Client]
	public void SetBuff(ElementButton skillElement, SOHabilidad skill) {
		if (skillElement != ElementButton.Air && skillElement != ElementButton.Water) return;
		
		if (!actuallyBuffedOrDebuffed) {
			actuallyBuffedOrDebuffed = true;
			StartCoroutine(BuffsAndDebuffs(skillElement, skill));
		}
	}

	private Coroutine onFireCor; //Corrutina de que está quemando
	/// <summary>
	/// Pone todos los buffs y debuffs (en base a lo que recibe la función)
	/// Al cabo del tiempo que diga la habilidad resetea todo
	/// </summary>
	/// <param name="elementOfBuff">Elemento que va a aplicar</param>
    private IEnumerator BuffsAndDebuffs(ElementButton elementOfBuff, SOHabilidad skill){
        float baseSpeed = manager.speed;
        if (elementOfBuff == ElementButton.Earth)
            manager.speed *= skill.slow;

        if (elementOfBuff == ElementButton.Fire)
            onFireCor = StartCoroutine(OnFire(skill.quemadura));
        
        if(elementOfBuff == ElementButton.Air)
            manager.speed *= skill.masVelocidad;

        if(elementOfBuff == ElementButton.Water)
            manager.DMGBase += skill.masDamage;   

        yield return new WaitForSeconds(skill.buffent);

        if (elementOfBuff == ElementButton.Earth)
            manager.speed = baseSpeed;

        if (elementOfBuff == ElementButton.Fire)
            StopCoroutine(onFireCor);

        if(elementOfBuff == ElementButton.Air)
            manager.speed = baseSpeed;
        
        if(elementOfBuff == ElementButton.Water)
            manager.DMGBase -= skill.masDamage;

        actuallyBuffedOrDebuffed = false;
    }

	/// <summary>
	/// Hace daño cada X tiempo ignorando la invlunerabilidad al nosotros mismos como si fuese quien nos quemó
	/// El while no es problema porque esto se guarda en una corrutina y al acabar su tuempo de actuación se para
	/// </summary>
	private IEnumerator OnFire(float dmg) {
		while (true) {
			yield return new WaitForSeconds(.25f);
			Hit(dmg, penguinWhoBurntMe, true);
		}
	}

	/// <summary>
	/// Gasta dinero
	/// </summary>
	/// <returns>Devuelve si puede o no, en caso de poder devuelve true y lo gasta</returns>
	public bool SpendMoney(int cost) {
        if(!isLocalPlayer) return false;
        if(money >= cost) {
            SpendMoneyCMD(cost);
            return true;
        } else
            return false;
    }

    [Command]
    private void SpendMoneyCMD(int cost) {
        money -= cost;
    }
}
