using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SOHabilidad))]
public class SOHabiliadesEditor : Editor {

	SOHabilidad coso;

	public override void OnInspectorGUI() {
		GUIStyle letraGordita = new GUIStyle() {
			alignment = TextAnchor.MiddleCenter,
			fontSize = 20,
			fontStyle = FontStyle.Bold
		};
		letraGordita.normal.textColor = Color.white;

		GUIStyle letraNormal = new GUIStyle() {
			alignment = TextAnchor.MiddleCenter,
			fontSize = 11,
			fontStyle = FontStyle.Bold
		};
		letraNormal.normal.textColor = Color.white;

		GUIStyle backgroundColored = new GUIStyle() {
			alignment = TextAnchor.MiddleCenter,
			margin = new RectOffset(5,5,0,0),
			padding = new RectOffset(2, 2, 2, 2),
			fontSize = 15,
			fontStyle = FontStyle.Bold,
		};
		backgroundColored.normal.textColor = Color.white;
		backgroundColored.normal.background = MakeTex(30, 1, new Color(1.0f, 1.0f, 1.0f, 0.1f));

		GUIStyle overflowTextArea = new GUIStyle() {
			margin = new RectOffset(5, 5, 0, 0),
			padding = new RectOffset(5, 5, 5, 5),
			fontSize = 13,
			wordWrap = true,
		};
		overflowTextArea.normal.textColor = Color.white;
		overflowTextArea.normal.background = MakeTex(30, 1, new Color(1.0f, 1.0f, 1.0f, 0.05f));

		EditorGUILayout.LabelField(coso.name, letraGordita);

		Spacear(3);

		#region BaseParams
		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Nombre habilidad", letraNormal);
		coso.name = EditorGUILayout.TextField(coso.name);
		EspacioInterno();
		GUILayout.Label("Objeto a spawnear", letraNormal);
		EspacioInterno();
		coso.objectToSpawn = (GameObject)EditorGUILayout.ObjectField(coso.objectToSpawn, typeof(GameObject), true);
		EditorGUILayout.EndHorizontal();

		Spacear();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label(new GUIContent("Prim. El.", "Elemento principal"), letraNormal);
		coso.element1 = (ElementButton)EditorGUILayout.EnumPopup(coso.element1);
		EspacioInterno();
		switch (coso.element1) {
			case ElementButton.Fire:
				GUILayout.Label("Daño de quemadura", letraNormal);
				coso.quemadura = EditorGUILayout.FloatField(coso.quemadura);
				break;
			case ElementButton.Water:
				GUILayout.Label("Aumento de daño", letraNormal);
				coso.masDamage = EditorGUILayout.Slider(coso.masDamage, 1, 2);
				break;
			case ElementButton.Earth:
				GUILayout.Label("Ralentización", letraNormal);
				coso.slow = EditorGUILayout.Slider(coso.slow, 0, 1);
				break;
			case ElementButton.Air:
				GUILayout.Label("Aumento de velocidad", letraNormal);
				coso.masVelocidad = EditorGUILayout.Slider(coso.masVelocidad, 1, 2);
				break;
		}
		EditorGUILayout.EndHorizontal();

		Spacear();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label(new GUIContent("Sec. El. ", "Elemento secundario"), letraNormal);
		coso.element2 = (ElementButton)EditorGUILayout.EnumPopup(coso.element2);
		EspacioInterno();
		switch (coso.element2) {
			case ElementButton.Fire:
				GUILayout.Label("Daáo de quemadura", letraNormal);
				coso.quemadura = EditorGUILayout.FloatField(coso.quemadura);
				break;
			case ElementButton.Water:
				GUILayout.Label("Aumento de daáo", letraNormal);
				coso.masDamage = EditorGUILayout.Slider(coso.masDamage, 1, 2);
				break;
			case ElementButton.Earth:
				GUILayout.Label("Ralentización", letraNormal);
				coso.slow = EditorGUILayout.Slider(coso.slow, 0, 1);
				break;
			case ElementButton.Air:
				GUILayout.Label("Aumento de velocidad", letraNormal);
				coso.masVelocidad = EditorGUILayout.Slider(coso.masVelocidad, 1, 2);
				break;
			default:
				break;
		}
		EditorGUILayout.EndHorizontal();

		Spacear();

		EditorGUILayout.BeginHorizontal();
		EspacioInterno(2);
		GUILayout.Label(new GUIContent("Duración de los bufos", "Cuando duran los bufos y debufos de esta habilidad"), letraNormal);
		coso.buffent = EditorGUILayout.FloatField(coso.buffent, GUILayout.MaxWidth(25));
		EspacioInterno();
		EditorGUILayout.EndHorizontal();


		#endregion

		Spacear(5);

		#region Stats

		EditorGUILayout.LabelField("Stats", letraGordita);
		Spacear(3);

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label(new GUIContent("Coste", "Coste de maná"), letraNormal);
		coso.manaCost = EditorGUILayout.FloatField(coso.manaCost);
		EspacioInterno();
		GUILayout.Label("Nivel actual de la habilidad", letraNormal);
		coso.nivelHabilidad = EditorGUILayout.IntPopup(coso.nivelHabilidad, 
			new string[] { "1", "2","3","4","5" }, new int[] { 1, 2, 3, 4, 5 });
		EditorGUILayout.EndHorizontal();

		Spacear();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("Daño ", letraNormal);
		coso.damageHabilidad = EditorGUILayout.FloatField(coso.damageHabilidad);
		EspacioInterno(14);
		GUILayout.Label(new GUIContent("Rango del area de uso", "Rango del circulo donde se puede lanzar esta habilidad"), letraNormal);
		coso.maxArea = EditorGUILayout.Slider(coso.maxArea, 0, 25);

		EditorGUILayout.EndHorizontal();

		Spacear();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label(new GUIContent("Area de efecto", "True = Seleccionas dentro del area donde quieres que se ejecute la habilidad"), letraNormal);
		coso.mustSelectWhere = EditorGUILayout.Toggle(coso.mustSelectWhere);
		GUILayout.Label(new GUIContent("Quieto durante lanzamiento", "True = Te quedas quieto el 'Tiempo de casteo'"), letraNormal);
		coso.stillOnCast = EditorGUILayout.Toggle(coso.stillOnCast);
		EditorGUILayout.EndHorizontal();

		if (coso.stillOnCast) {
			Spacear();

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label(new GUIContent("Tiempo de casteo", "Tiempo que tardas en poder volver a moverte en caso de estar activado el 'Quieto durante lanzamiento'"), letraNormal);
			coso.moveAgainTime = EditorGUILayout.FloatField(coso.moveAgainTime);

			GUILayout.Label(new GUIContent("Tiempo para volver a castear", "Tiempo (extra a parte de un segundo) que tardas en volver a poder lanzar una habilidad"), letraNormal);
			coso.castAgainExtraTime = EditorGUILayout.FloatField(coso.castAgainExtraTime);
			EditorGUILayout.EndHorizontal();
		}

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label(new GUIContent("Coste de upgradear", "Dinero que cuesta comprar un upgrade de la habilidad en la tienda "), letraNormal);
        coso.moneyCostUpgrade = EditorGUILayout.FloatField(coso.moneyCostUpgrade);
        EditorGUILayout.EndHorizontal();
        Spacear();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Descripcción", letraNormal);
		coso.skillDescription = EditorGUILayout.TextArea(coso.skillDescription, overflowTextArea, GUILayout.ExpandHeight(true));
        EditorGUILayout.EndHorizontal();


        #endregion

        Spacear(5);

		#region HUD

		EditorGUILayout.LabelField("HUD", letraGordita);
		Spacear(3);

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label(new GUIContent("Kanji", "Carácter del elemento"), letraNormal);
		coso.kanji = EditorGUILayout.TextArea(coso.kanji,backgroundColored);
		EspacioInterno();
		GUILayout.Label(new GUIContent("Color de fondo", "Color principal del fondo del HUD de esta habilidad"), letraNormal);
		coso.backgroundColor = EditorGUILayout.ColorField(GUIContent.none, coso.backgroundColor,true,true,true, GUILayout.MinWidth(50), GUILayout.ExpandWidth(true));
		EditorGUILayout.EndHorizontal();

		Spacear();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Label(new GUIContent("Color del kanji", "Color del carácter dentro del HUD"), letraNormal);
		coso.kanjiStrokeColor = EditorGUILayout.ColorField(GUIContent.none, coso.kanjiStrokeColor, true, true, true, GUILayout.MinWidth(50), GUILayout.ExpandWidth(true));
		EspacioInterno();
		GUILayout.Label(new GUIContent("Color del borde", "Color del borde, normalmente es el background pero más oscuro"), letraNormal);
		coso.outlineColor = EditorGUILayout.ColorField(GUIContent.none, coso.outlineColor, true, true, true, GUILayout.MinWidth(50), GUILayout.ExpandWidth(true));
		EditorGUILayout.EndHorizontal();

		Spacear();
		GUILayout.Label(new GUIContent("Color de la habilidad", "Es el color del cristal de la bara, de la luz y de las estrellas al tener la habilidad seleccionada"), letraNormal);
		coso.staffColor = EditorGUILayout.ColorField(GUIContent.none, coso.staffColor, true, true, true, GUILayout.MinWidth(50), GUILayout.ExpandWidth(true));


		#endregion

	}
	private void EspacioInterno(float ancho = 10) {
		GUILayout.Label("", GUILayout.MinWidth(ancho));
	}
	private Texture2D MakeTex(int width, int height, Color col) {
		Color[] pix = new Color[width * height];

		for (int i = 0; i < pix.Length; i++)
			pix[i] = col;

		Texture2D result = new Texture2D(width, height);
		result.SetPixels(pix);
		result.Apply();

		return result;
	}


	private void Spacear(int q = 1) {
		for (int i = 0; i < q; i++) {
			EditorGUILayout.Space();
		}
	}

	private void OnEnable() {
		coso = (SOHabilidad)target;
	}
}
