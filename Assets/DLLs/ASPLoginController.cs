using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharedLibrary;
using SharedLibrary.Requests;
using SharedLibrary.Responses;
using TMPro;
using UnityEngine;


namespace Atherox.BackEnd {
    public class ASPLoginController : MonoBehaviour {
        [SerializeField] private TMP_InputField userField;
        [SerializeField] private TMP_InputField nicknameField;
        [SerializeField] private TMP_InputField passField;

        [SerializeField] private GameObject messageBox;
        private TextMeshProUGUI messageText;

        private GameObject[] menuItems;
        private int actualMenu = 1;

        private string JWTBearer;

        void Start() {
            SceneUtils.Instance.cannotClick?.SetActive(false);
            messageText = messageBox.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            messageBox.SetActive(false);

            Transform menuHolder = transform.GetChild(0);
            menuHolder.GetChild(0).gameObject.SetActive(true);
            menuItems = new GameObject[menuHolder.childCount - 1];
            for (int i = 1; i < menuHolder.childCount; i++) {
                menuItems[i - 1] = menuHolder.GetChild(i).gameObject;
                menuItems[i - 1].SetActive(i - 1 == 0);
            }
        }

        #region REGISTER

        public async void RegisterButton() {
            if (passField.text.Length < 3 || userField.text.Length < 3) {
                SetBoxMessage("Error, Username and Password must be longer than 3 characters", Color.red);
                return;
            }

            SceneUtils.Instance.cannotClick?.SetActive(true);
            await RegisterAction();
            SceneUtils.Instance.cannotClick?.SetActive(false);
        }

        private async Task RegisterAction() {
            AuthRequests authRequest = new AuthRequests() { Username = userField.text, Password = passField.text };
            // ReSharper disable once HeapView.BoxingAllocation
            AuthResponse authResponse = null;
            try {
                authResponse = await HttpClient.Post<AuthResponse>(Endpoints.AuthRegister.Info(), authRequest);
            } catch (EndpointException e) {
                Debug.LogException(e);
                SetBoxMessage(e.Message, Color.red);
                return;
            }

            JWTBearer = authResponse.token;
            Debug.Log("[Logister] - Register of User(AUTH) done");

            NextMenu();
        }

        public async void SubmitNicknameButton() {
            if (nicknameField.text.Length < 3) {
                SetBoxMessage("Error, Nickname must be longer than 3 characters", Color.red);
                return;
            }

            SceneUtils.Instance.cannotClick?.SetActive(true);
            await SubmitNicknameAction();
            SceneUtils.Instance.cannotClick?.SetActive(true);
        }

        private async Task SubmitNicknameAction() {
            CreatePlayerRequest playerRequest = new CreatePlayerRequest() { Name = nicknameField.text };

            PlayerResponse player = null;
            try {
                player = await HttpClient.Post<PlayerResponse>(Endpoints.PlayerRegister.Info(), playerRequest,
                    bearerKey: JWTBearer);
            } catch (EndpointException e) {
                Debug.LogException(e);
                SetBoxMessage(e.Message, Color.red);
                return;
            }

            SceneUtils.Instance.player = new Player() {
                Experience = player.Experience,
                Id = player.Id,
                Level = player.Level,
                Name = player.Name
            };
            Debug.Log("[Logister] - Register of Player done");
        }

        #endregion

        #region LOGIN

        public async void LoginButton() {
            if (userField.text.Length < 3 || passField.text.Length < 3) {
                SetBoxMessage("Error, Username and Password must be longer than 3 characters", Color.red);
                return;
            }

            SceneUtils.Instance.cannotClick?.SetActive(true);
            await LoginAction();
            SceneUtils.Instance.cannotClick?.SetActive(false);
        }

        private async Task LoginAction() {
            if (JWTBearer == null) {
                AuthRequests authRequest = new AuthRequests() { Username = userField.text, Password = passField.text };
                // ReSharper disable once HeapView.BoxingAllocation
                AuthResponse authResponse = null;
                try {
                    authResponse = await HttpClient.Post<AuthResponse>(Endpoints.AuthLogin.Info(), authRequest);
                } catch (EndpointException e) {
                    Debug.LogException(e);
                    SetBoxMessage(e.Message, Color.red);
                    return;
                }

                JWTBearer = authResponse.token;
                Debug.Log("[Logister] - Register of User(AUTH) done");
            }
            PlayerResponse player = null;
            try {
                player = await HttpClient.Get<PlayerResponse>(Endpoints.PlayerGet.Info(), bearerKey: JWTBearer);
            } catch (EndpointException e) {
                Debug.LogException(e);
                SetBoxMessage(e.Message, Color.red);
                return;
            }

            SceneUtils.Instance.player = new Player() {
                Experience = player.Experience,
                Id = player.Id,
                Level = player.Level,
                Name = player.Name
            };
            Debug.Log("[Logister] - Login of Player done");

        }

        #endregion

        #region Message

        private void SetBoxMessage(string msg, Color color, float time = 2f) {
            messageBox.SetActive(true);
            messageText.text = msg;
            ShowMessage(time, color);
        }

        private Color alpha = new Color(0, 0, 0, 0);

        private async Task ShowMessage(float time, Color color) {
            UnityEngine.UI.Outline messageOutl = messageBox.GetComponent<UnityEngine.UI.Outline>();
            for (float t = 1; t > 0; t -= Time.deltaTime) {
                messageOutl.effectColor = Color.Lerp(color, alpha, t);
                messageText.color = Color.Lerp(Color.white, alpha, t);
                await Task.Yield();
            }

            await Task.Delay(2000);

            for (float t = 0; t < 1; t += Time.deltaTime / time) {
                messageOutl.effectColor = Color.Lerp(color, alpha, t);
                messageText.color = Color.Lerp(Color.white, alpha, t);
                await Task.Yield();
            }

            messageBox.SetActive(false);
        }

        #endregion

        private void NextMenu() {
            actualMenu++;
            foreach (var t in menuItems) {
                t.SetActive(t.transform.GetSiblingIndex() == actualMenu);
            }
        }
    }


    public enum Endpoints {
        [Endpoint("https://localhost:7243/Auth/register")]
        AuthRegister,

        [Endpoint("https://localhost:7243/Auth/login")]
        AuthLogin,

        [Endpoint("https://localhost:7243/Player/register")]
        PlayerRegister,

        [Endpoint("https://localhost:7243/Player/get")]
        PlayerGet,
    }

    public class Endpoint : Attribute {
        public string text { get; set; }

        public Endpoint(String _text) {
            text = _text;
        }
    }

    public static class OverrideEndpoints {
        public static string Info(this Enum en) {
            Type type = en.GetType();
            MemberInfo[] memberInfo = type.GetMember(en.ToString());
            if (memberInfo == null || memberInfo.Length == 0)
                return en.ToString();

            object[] attribs = memberInfo[0].GetCustomAttributes(
                typeof(Endpoint), false);
            if (attribs != null && attribs.Length > 0)
                return ((Endpoint)attribs[0]).text;

            return en.ToString();
        }
    }
}